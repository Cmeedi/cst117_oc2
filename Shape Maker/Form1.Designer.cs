﻿namespace Shape_Maker
{
    partial class ShapeMakerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Warrior"}, 0, System.Drawing.Color.Empty, System.Drawing.SystemColors.HighlightText, null);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Ranger", 1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Rogue", 2);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Wizard", 3);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Sorcerer", 4);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Mage", 5);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Necormancer", 6);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("Paladin", 7);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("Priest", 8);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShapeMakerForm));
            this.ClassListBox = new System.Windows.Forms.ListView();
            this.ClassSelectLabel = new System.Windows.Forms.Label();
            this.MeleeRadioButton = new System.Windows.Forms.RadioButton();
            this.RangeRadioButton = new System.Windows.Forms.RadioButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.ClassTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.NonMagicRadioButton = new System.Windows.Forms.RadioButton();
            this.AllRadioButton = new System.Windows.Forms.RadioButton();
            this.MagicRadioButton = new System.Windows.Forms.RadioButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ClassTypeGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClassListBox
            // 
            this.ClassListBox.HideSelection = false;
            listViewItem1.Checked = true;
            listViewItem1.StateImageIndex = 1;
            this.ClassListBox.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9});
            this.ClassListBox.Location = new System.Drawing.Point(134, 32);
            this.ClassListBox.Name = "ClassListBox";
            this.ClassListBox.Size = new System.Drawing.Size(136, 228);
            this.ClassListBox.SmallImageList = this.imageList1;
            this.ClassListBox.TabIndex = 1;
            this.ClassListBox.UseCompatibleStateImageBehavior = false;
            this.ClassListBox.View = System.Windows.Forms.View.SmallIcon;
            this.ClassListBox.SelectedIndexChanged += new System.EventHandler(this.ClassList_SelectedIndexChanged);
            // 
            // ClassSelectLabel
            // 
            this.ClassSelectLabel.AutoSize = true;
            this.ClassSelectLabel.Location = new System.Drawing.Point(131, 12);
            this.ClassSelectLabel.Name = "ClassSelectLabel";
            this.ClassSelectLabel.Size = new System.Drawing.Size(85, 17);
            this.ClassSelectLabel.TabIndex = 2;
            this.ClassSelectLabel.Text = "Select Class";
            // 
            // MeleeRadioButton
            // 
            this.MeleeRadioButton.AutoSize = true;
            this.MeleeRadioButton.Location = new System.Drawing.Point(6, 21);
            this.MeleeRadioButton.Name = "MeleeRadioButton";
            this.MeleeRadioButton.Size = new System.Drawing.Size(67, 21);
            this.MeleeRadioButton.TabIndex = 3;
            this.MeleeRadioButton.TabStop = true;
            this.MeleeRadioButton.Text = "Melee";
            this.MeleeRadioButton.UseVisualStyleBackColor = true;
            // 
            // RangeRadioButton
            // 
            this.RangeRadioButton.AutoSize = true;
            this.RangeRadioButton.Location = new System.Drawing.Point(6, 48);
            this.RangeRadioButton.Name = "RangeRadioButton";
            this.RangeRadioButton.Size = new System.Drawing.Size(79, 21);
            this.RangeRadioButton.TabIndex = 4;
            this.RangeRadioButton.TabStop = true;
            this.RangeRadioButton.Text = "Ranged";
            this.RangeRadioButton.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 190);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(98, 21);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(12, 226);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(98, 21);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // ClassTypeGroupBox
            // 
            this.ClassTypeGroupBox.Controls.Add(this.MagicRadioButton);
            this.ClassTypeGroupBox.Controls.Add(this.AllRadioButton);
            this.ClassTypeGroupBox.Controls.Add(this.NonMagicRadioButton);
            this.ClassTypeGroupBox.Controls.Add(this.RangeRadioButton);
            this.ClassTypeGroupBox.Controls.Add(this.MeleeRadioButton);
            this.ClassTypeGroupBox.Location = new System.Drawing.Point(12, 12);
            this.ClassTypeGroupBox.Name = "ClassTypeGroupBox";
            this.ClassTypeGroupBox.Size = new System.Drawing.Size(113, 156);
            this.ClassTypeGroupBox.TabIndex = 8;
            this.ClassTypeGroupBox.TabStop = false;
            this.ClassTypeGroupBox.Text = "Class Type";
            // 
            // NonMagicRadioButton
            // 
            this.NonMagicRadioButton.AutoSize = true;
            this.NonMagicRadioButton.Location = new System.Drawing.Point(6, 102);
            this.NonMagicRadioButton.Name = "NonMagicRadioButton";
            this.NonMagicRadioButton.Size = new System.Drawing.Size(97, 21);
            this.NonMagicRadioButton.TabIndex = 8;
            this.NonMagicRadioButton.TabStop = true;
            this.NonMagicRadioButton.Text = "Non-Magic";
            this.NonMagicRadioButton.UseVisualStyleBackColor = true;
            // 
            // AllRadioButton
            // 
            this.AllRadioButton.AutoSize = true;
            this.AllRadioButton.Location = new System.Drawing.Point(6, 129);
            this.AllRadioButton.Name = "AllRadioButton";
            this.AllRadioButton.Size = new System.Drawing.Size(44, 21);
            this.AllRadioButton.TabIndex = 9;
            this.AllRadioButton.TabStop = true;
            this.AllRadioButton.Text = "All";
            this.AllRadioButton.UseVisualStyleBackColor = true;
            // 
            // MagicRadioButton
            // 
            this.MagicRadioButton.AutoSize = true;
            this.MagicRadioButton.Location = new System.Drawing.Point(6, 75);
            this.MagicRadioButton.Name = "MagicRadioButton";
            this.MagicRadioButton.Size = new System.Drawing.Size(66, 21);
            this.MagicRadioButton.TabIndex = 10;
            this.MagicRadioButton.TabStop = true;
            this.MagicRadioButton.Text = "Magic";
            this.MagicRadioButton.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Warrior");
            this.imageList1.Images.SetKeyName(1, "Ranger");
            this.imageList1.Images.SetKeyName(2, "Rogue");
            this.imageList1.Images.SetKeyName(3, "Wizard");
            this.imageList1.Images.SetKeyName(4, "Sorceress");
            this.imageList1.Images.SetKeyName(5, "Mage");
            this.imageList1.Images.SetKeyName(6, "Necromancer");
            this.imageList1.Images.SetKeyName(7, "Paladin");
            this.imageList1.Images.SetKeyName(8, "Priest");
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.ImageList = this.imageList1;
            this.label1.Location = new System.Drawing.Point(300, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 228);
            this.label1.TabIndex = 9;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(369, 275);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(90, 29);
            this.OKButton.TabIndex = 10;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(273, 275);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 29);
            this.CancelButton.TabIndex = 11;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // ShapeMakerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 316);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ClassTypeGroupBox);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.ClassSelectLabel);
            this.Controls.Add(this.ClassListBox);
            this.Name = "ShapeMakerForm";
            this.Text = "Shape Maker";
            this.ClassTypeGroupBox.ResumeLayout(false);
            this.ClassTypeGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ClassListBox;
        private System.Windows.Forms.Label ClassSelectLabel;
        private System.Windows.Forms.RadioButton MeleeRadioButton;
        private System.Windows.Forms.RadioButton RangeRadioButton;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox ClassTypeGroupBox;
        private System.Windows.Forms.RadioButton AllRadioButton;
        private System.Windows.Forms.RadioButton NonMagicRadioButton;
        private System.Windows.Forms.RadioButton MagicRadioButton;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
    }
}

